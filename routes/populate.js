var express = require('express');
var router = express.Router();
var Offre = require('../models/offre');
var mongoose = require('mongoose');


/* populate test data */
router.get('/', function(req, res, next) {
    Offre.count({}, function( err, count){
        if (count == 0) {
           var offres = [];
           // ajoute 10 offres
            for (var i = 0; i < 10; i++) {
                var m=1;
                var a=2017;
                var offre = new Offre({
                _id: new mongoose.Types.ObjectId(),
                nomEvent: "Joe"+i,
                typeEvent: "film" + i,
                dateStart: i+"/"+m+"/"+a
                })
            offres.push(offre);
        }
        Offre.collection.insert(offres, function(err, docs) {
            if (err) {
            // TODO: handle error
            } else {
                res.send('creation des utilisateurs de test réussie');
            }
        });
        } else {
            res.send('creation des utilisateurs de non effectuée (document non vide)');
        }
    });
});

module.exports = router;
