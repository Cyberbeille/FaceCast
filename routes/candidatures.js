var express = require('express');
var router = express.Router();
var Offre = require('../models/offre');
var Candidature = require('../models/candidature');
var Figurant = require('../models/figurant');

router.get("/", function(req, res, next){

    //jointure entre la table offre et figurant
Candidature.aggregate([
    {
      $lookup:
        {
          from: "offres",
          localField: "offre",
          foreignField: "_id",
          as: "offer"
        }
   },


    {
      $lookup:
        {
          from: "figurants",
          localField: "figurants",
          foreignField: "_id",
          as: "person"
        }
   }
    
]).exec(function (err, lescandidatures) {
    if (err) throw err;
    res.render('candidatures', { title: 'Les Candidatures', candidatures: lescandidatures });
    
  });
});   

// Update etat de la candidature
router.post('/update/:id', function (req, res, next) {
  Candidature.findByIdAndUpdate(req.params.id, {
    etat: req.body.etat
  }, function (err, doc) {
    if (err) {
      console.log('error');
    }
    else {
      res.redirect('/candidatures');
      console.log(doc);
    }
  });
});

    
module.exports = router;