var express = require('express');
var router = express.Router();
var Figurant = require('../models/figurant');


//route pour l'existance de la page figurants.pug
router.get('/', function(req, res, next) {
  Figurant.find({}, function(err, lesFigurants) {
     if (err) throw err;
     res.render('figurants', { title: 'Les Figurants', figurants: lesFigurants });
  });
    
});

//route pour l'existance de la page addfigurant.pug
router.get('/addfigurant', function (req, res){
    
    res.render('addfigurant');
});

//route permettant la recupération du formulaire addfigurant ainsi que son écriture dans mongoDB 
router.post('/', function(req,res){
  var figurant = new Figurant({
    _id: req.body._id,
    lastname: req.body.lastname,
    firstname: req.body.firstname, 
    email: req.body.email
  })
  Figurant.collection.insert(figurant, function(err, docs){
    if(err){
      res.send("Erreur");
      console.log(err);
    } else{
        console.log("Figurant ajoutée !");
        res.redirect("/figurants");
    }
    
  });
});


router.get('/delete/:id', function (req, res) {
  Figurant.findByIdAndRemove(req.params.id, function (err, obj) {
    if (err) {
      console.log(err)
      res.send("Erreur");
    } else {
      console.log("Figurants suprrimés !")
      res.redirect("/figurants");
    }
  });
});

router.get('/update/:id', function (req, res){
    
    Figurant.findById(req.params.id, function(err, unefigurant){
    if (err) throw err;
    res.render('UpdateFig',{ title: 'Modifier Votre Figurant', figurant: unefigurant
        });
    });
});

// Route pour la mise a jour d’un figurant
// Route pour la modification d’un figurant
router.post('/update/edit/:id', function (req, res) {
  Figurant.findByIdAndUpdate(req.params.id, 
    { $set:req.body}, function (err, docs) {
    if (err) {
      console.log(err)
      res.send("Erreur");
    } else {
      console.log("Figurant modifiée !")
      res.redirect("/figurants");
    }
  });
});

module.exports = router;