var express = require('express');
var router = express.Router();
var Offre = require('../models/offre');


router.get('/', function(req, res, next) {
  Offre.find({}, function(err, lesOffres) {
     if (err) throw err;
     res.render('offres', { title: 'Nos Offres', offres: lesOffres });
  });
    
});

router.get('/addoffre', function (req, res){
    
    res.render('addoffre');
});

router.post('/', function(req,res){
  var offre = new Offre({
    _id: req.body._id,
    nomEvent: req.body.nomEvent,
    typeEvent: req.body.typeEvent, 
    dateStart: req.body.dateStart,
    nbJours: req.body.nbJours,
    roleDem: req.body.roleDem,
    nbFig: req.body.nbFig,
  })
  Offre.collection.insert(offre, function(err, docs){
    if(err){
      res.send("Erreur");
      console.log(err);
    } else{
        console.log("Offre ajoutée !");
        res.redirect("/offres");
    }
    
  });
});

router.get('/delete/:id', function (req, res) {
  Offre.findByIdAndRemove(req.params.id, function (err, obj) {
    if (err) {
      console.log(err)
      res.send("Erreur");
    } else {
      console.log("Offre suprrimée !")
      res.redirect("/offres");
    }
  });
});


router.get('/update/:id', function (req, res){
    
    Offre.findById(req.params.id, function(err, uneoffre){
    if (err) throw err;
    res.render('Updateform',{ title: 'Modifier Votre Offre', offre: uneoffre
        });
    });
});
    
// Route pour la mise a jour d’une offre
// Route pour la modification d’une offre
router.post('/update/edit/:id', function (req, res) {
  Offre.findByIdAndUpdate(req.params.id, 
    { $set:req.body}, function (err, docs) {
    if (err) {
      console.log(err)
      res.send("Erreur");
    } else {
      console.log("Offre modifiée !")
      res.redirect("/offres");
    }
  });
});





module.exports = router;