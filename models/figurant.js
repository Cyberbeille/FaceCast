var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var figurantSchema = Schema({
    _id: Schema.Types.ObjectId,
    lastname: String,
    firstname: String,
    email: String,
    key: String,
    candidatures: [{ type: Schema.Types.ObjectId, ref:'Candidature' }],
});

var Figurant = mongoose.model('Figurant', figurantSchema);
// export le modèle
module.exports = Figurant;
