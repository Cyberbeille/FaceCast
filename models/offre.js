var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var offreSchema = Schema({
    _id: Schema.Types.ObjectId,
    nomEvent: String,
    typeEvent: String,
    dateStart: Date.parse('DD-MM-YY'),
    nbJours: Number,
    roleDem: String,
    nbFig: Number,
    candidatures: [{ type: Schema.Types.ObjectId, ref:'Candidature' }],
});

var Offre = mongoose.model('Offres', offreSchema);
// export le modèle
module.exports = Offre;