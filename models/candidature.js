var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var candidatureSchema = Schema({
    _id: Schema.Types.ObjectId,
    etat: String,
    figurant: { type: Schema.Types.ObjectId, ref:'Figurant' },
    offre: { type: Schema.Types.ObjectId, ref: 'Offre'},
});

var Candidature = mongoose.model('Candidature', candidatureSchema);
// export le modèle
module.exports = Candidature;
