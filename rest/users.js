var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/figurant');

// Retourne l'ensemble des utilisateurs
router.get('/', function(req, res, next) {
  User.find(function(err, users) {
    if (err) {
      res.send(err);
    }
  
    console.log('controleur rest/users.js !');
    res.json(users);
  });
});

// Retourne un utilisateur en fonction de sa clé
router.get('/:key', function(req, res, next) {
  User.find({'key': req.params.key}, function (err, post) {
    if (err) return next(err);

    console.log('controleur rest/users.js avec key !');
    res.json(post);
  });
});

module.exports = router;